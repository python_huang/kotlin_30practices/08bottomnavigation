package com.example.a8bottomnavigation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_other.*

class Activity_3 : AppCompatActivity() {
    private val intent = intent()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_other)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        init()
        other_button.setOnClickListener { intent.intent(this, MainActivity()) }
    }

    private fun init() {
        other_textView.text = "H2ME"
        other_button.text = "BYE"
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}