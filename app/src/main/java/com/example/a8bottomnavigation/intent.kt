package com.example.a8bottomnavigation

import android.content.Context
import android.content.Intent

class intent() {
    fun intent(from: Context?, to: Context) {
        val intent = Intent(from, to::class.java)
        from!!.startActivity(intent)
    }
}